

let num = Number(prompt("Give me a number: "))

console.log("The number you provided is: "+num)
for(;num >= 0; num--){
	if(num === 50){
		console.log("The current value is at 50. Terminating the loop")
		break;
	}
	if(num % 10 === 0){
		console.log("The number is divisible by 10. Skipping...");
		continue;
	}
	if(num % 5 === 0){
		console.log(num)
		continue;
	}
	
}

let word = "supercalifragilisticexpialidocious" 
let consonants;
console.log(word)
for(let i = 0; i < word.length; i++){
	if(
		word[i].toLowerCase() == "a" ||
		word[i].toLowerCase() == "e" ||
		word[i].toLowerCase() == "i" ||
		word[i].toLowerCase() == "o" ||
		word[i].toLowerCase() == "u"
	){
		continue;
	}else{
		consonants += word[i]
	}
}
console.log(consonants)